extends Node

# Settings related constants
const SETTINGS_FILE_PATH = "user://settings.cfg"

const MUSIC_BUS = 1
const SFX_BUS = 2

const DEFAULT_SETTINGS = {
	"main": {
		"lang": "es",
	},
	"audio": {
		"music": true,
		"sfx": true,
	},
	"display": {
		"font_size": 1,
	},
}

# Global constants
const CLICK_THRESHOLD = 10
const GAME_STATE_PATH = "user://gbe_state.sav"

# Gamebook engine constants
const GBE_PLUGINS_PATH = "res://scripts/gbe_plugins/"
const GBE_STORY_PATH = "res://story"
const GBE_MEDIA_PATH = "res://story/media"
const GBE_ITEM_DATA_PATH = "res://story/items"
const GBE_CHARACTER_DATA_PATH = "res://story/characters"
const GBE_INITIAL_MODULE = "res://story/main.gbe"

var settings = null
var debug = false
var start_section = null

func _ready():
	load_settings()
	
	# Detect if GBE_DEBUG variable is set
	debug = OS.get_environment('GBE_DEBUG')
	
	# Check if is there an starting section
	start_section = OS.get_environment('GBE_START_SECTION')


func load_settings():
	settings = ConfigFile.new()
	var err = settings.load(SETTINGS_FILE_PATH)
	# Store default settings if they hasn't been defined yet
	for section_name in DEFAULT_SETTINGS.keys():
		var section = DEFAULT_SETTINGS[section_name]
		for key in section.keys():
			if err != OK or not settings.has_section_key(section_name, key):
				set_setting(section_name, key, section[key])
	# Update game settings
	setup_settings()


func set_setting(section, key, value):
	settings.set_value(section, key, value)


func get_setting(section, key):
	return settings.get_value(section, key)


func save_settings():
	var music = not AudioServer.is_bus_mute(MUSIC_BUS)
	set_setting("audio", "music", music)
	var sfx = not AudioServer.is_bus_mute(SFX_BUS)
	set_setting("audio", "sfx", sfx)
	settings.save(SETTINGS_FILE_PATH)


func setup_settings():
	# Set audio volumes
	AudioServer.set_bus_mute(MUSIC_BUS, get_setting("audio", "music"))
	AudioServer.set_bus_mute(SFX_BUS, get_setting("audio", "sfx"))
	# Set language and restart current scene to translate it
	TranslationServer.set_locale(settings.get_value("main", "lang"))
	get_tree().reload_current_scene()
