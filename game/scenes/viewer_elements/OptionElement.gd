extends Button


export(int) var option_height = 100

var viewer = null
var option = null
var _cursor_pos = null


func init(viewer_instance, option_data):
	viewer = viewer_instance
	option = option_data
	var w = get_parent().get_size().x - 500
	set_text(option["text"])
	#set_custom_minimum_size(Vector2(w, option_height))
	rect_size = (Vector2(w, option_height))

func _on_OptionElement_button_down():
	_cursor_pos = get_viewport().get_mouse_position()


func _on_OptionElement_pressed():
	if (_cursor_pos - get_viewport().get_mouse_position()).length() < SettingsManager.CLICK_THRESHOLD:
		viewer._select_option(option["dest"], option["module"])

