extends RichTextLabel

const HORIZONTAL_MARGIN = 15
const VERTICAL_MARGIN = 0

var viewer = null
var _first_drawn = false

func init(viewer_instance, text):
	viewer = viewer_instance
	var w = get_parent().get_size().x - HORIZONTAL_MARGIN
	set_custom_minimum_size(Vector2(w, 0))
	parse_bbcode(text)
	_first_drawn = true
	# Activate selectable text if in debug mode
	if SettingsManager.debug:
		selection_enabled = true

func _on_TextElement_draw():
	if _first_drawn:
		var w = get_parent().get_size().x - HORIZONTAL_MARGIN
		var h = get_content_height() + VERTICAL_MARGIN
		set_custom_minimum_size(Vector2(w, h))
		_first_drawn = false

func _on_TextElement_meta_clicked(meta):
	GBE.process_line(meta)
