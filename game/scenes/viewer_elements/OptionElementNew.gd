extends Button

var viewer = null
var option = null
var _cursor_pos = null
var _first_drawn = false

var V_MARGIN = 60
var H_MARGIN = 120

func init(viewer_instance, option_data):
	viewer = viewer_instance
	option = option_data
	$Label.set_text(option["text"])
	_first_drawn = true

func _on_OptionElement_button_down():
	_cursor_pos = get_viewport().get_mouse_position()

func _on_OptionElement_pressed():
	if (_cursor_pos - get_viewport().get_mouse_position()).length() < SettingsManager.CLICK_THRESHOLD:
		viewer._select_option(option["dest"], option["module"])

func _on_Label_draw():
	if _first_drawn:
		var w = get_parent().get_size().x
		var label_size = $Label.get_size()
		var new_label_size = Vector2(w - H_MARGIN, label_size.y)
		$Label.set_custom_minimum_size(new_label_size)
		$Label.set_size(new_label_size)
		var h = new_label_size.y + V_MARGIN
		set_custom_minimum_size(Vector2(w, h))
		$Label.rect_position = Vector2(H_MARGIN / 2, 0)
	_first_drawn = false